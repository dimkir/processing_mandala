class circle {
  PVector center;
  float radius;
  PVector spot; // the brush tip
  PVector bigC; // center of big circle
  float bigR; // big circle radius
  float theta;
  float acc;
  color R, G, B;
  int thFactor;

  // each circle has it's own number of petals
  circle(float r, PVector bc, 
  float bigr, float th, float ac
    ) {
    theta = th;
    acc = ac;
    center = new PVector(0.0, 0.0);
    radius = r;
    spot = new PVector(0.0, 0.0);
    bigC = new PVector(bc.x, bc.y);
    bigR = bigr;
    R = floor(random(100, 255));
    G = floor(random(100, 255));
    B = floor(random(100, 255));
    thFactor = floor(random(6, 13));
    update();
  }
  
  // all circle have the same number of petals
  circle(float r, PVector bc, 
  float bigr, float th, float ac, int tfactor
    ) {
    theta = th;
    acc = ac;
    center = new PVector(0.0, 0.0);
    radius = r;
    spot = new PVector(0.0, 0.0);
    bigC = new PVector(bc.x, bc.y);
    bigR = bigr;
    R = floor(random(100, 255));
    G = floor(random(100, 255));
    B = floor(random(100, 255));
    thFactor = tfactor;
    update();
  }

  void update() {
    theta+=acc;
    PVector moveto = new PVector(cos(theta), sin(theta), 0);
    moveto.normalize();
    moveto.mult((bigR-radius)/2);
    center= PVector.add(bigC, moveto);
    moveto.set(cos(theta*thFactor), sin(theta*thFactor));
    moveto.normalize();
    moveto.mult(-(radius/2));
    spot= PVector.add(center, moveto);
  }

  void drawit() {
    pushStyle();

    noFill();
    strokeWeight(2);
    stroke(R, G, B, 50);
    line(center.x, center.y, spot.x, spot.y);
    ellipse(spot.x, spot.y, 4, 4);

    popStyle();
  }
}



